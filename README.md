# Meet Me Backend Project

## Prerequisites

You need the following tools installed:

* nodejs [official website](https://nodejs.org/en/) - nodejs includes [npm](https://www.npmjs.com/) (node package manager)
* mongodb [official installation guide](https://docs.mongodb.org/manual/administration/install-community/)

## Setup (before first run)

In the root folder of this project you have to run the following commands:

**install node dependencies**

```
npm install
```

**set up your database**

* create a new directory where your database will be stored (e.g. config/db)
* start the database server 
```
mongod --dbpath relative/path/to/database
```
* create all database schemes and import data to begin with 
```
mongorestore dump/
```

**set up environment configuration**

copy the `config.dev_local.js` in the config directory and rename it to `config.js`. DO NOT check in your config.js file into source control. If you make a changes that your team members should be able to see (e.g. introducing a new config variable), change it in `config.dev_local.js`

You can also create more example config files in your `config` directory, e.g. `config.dev_server` for your development server. 

Note: While it is a good idea to have some configuration available for everyone, it is considered bad practice to check in sensitive data into source control (e.g. credentials for database access)

## Start the Server

start the web server

```
node server.js
```

Now the backend server should be up and running. You can verify that by opening http://localhost:3000/
