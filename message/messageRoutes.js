/**
 * Routes definitions for Message.
 */

module.exports = function (passport) {
    var messageController = require('./messageController');
    var router = require('express').Router();

    var middleware = passport.authenticate('jwt', {session: false});
    router.use(middleware);

    router.route('/:itemId')
        .post(messageController.postMessage)
        .get(messageController.getMessages);

    return router;
};
