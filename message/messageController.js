/**
 * Controller for messages.
 */

var Message = require('./messageSchema');
var Item = require('../item/itemSchema');
var Offer = require('../offer/offerSchema');
var Request = require('../request/requestSchema');

/**
 * POST /api/messages/:itemId
 */
exports.postMessage = function (req, res) {
    Item.findById(req.params.itemId, function (err, item) {
        if (err) {
            res.statusCode(404).send(err);
            return;
        }

        if (checkPermission(item, req.user)) {
            var msg = req.body;
            msg._item = req.params.itemId;
            msg._sender = req.user._id;
            msg.timestamp = Date.now();

            var message = new Message(msg);

            message.save(function (err, message) {
                if (err) {
                    res.status(500).send(err);
                    return;
                }
                res.status(201).json(message);
            });
        } else {
            res.status(403).send('You are not creator or acceptor of this item!');
        }
    });
};

/**
 * GET /api/messages/:itemId
 */
exports.getMessages = function (req, res) {
    Item.findById(req.params.itemId, function (err, item) {
        if (err) {
            res.status(404).send(err);
            return;
        }

        if (!checkPermission(item, req.user)) {
            res.status(403).send('You are not creator or acceptor of this item!');
            return;
        }

        Message
            .where('_item').equals(req.params.itemId)
            .sort({date: 'descending'})
            .populate('_sender')
            .exec(function (err, messages) {
                if (err) {
                    res.status(500).send(err);
                    return;
                }

                res.json(messages);
            });
    });
};

/**
 * Checks whether the given user is accepter or creator of the given item and therefore allowed to read and send
 * messages.
 *
 * @param user The user to check
 * @param item The item to check
 *
 * @return {Boolean}
 */
function checkPermission(item, user) {
    // user is creator of item
    if (item._creator.equals(user._id)) {
        return true;
    }

    // user is one accepter of offer
    if (item instanceof Offer && item._accepters.indexOf(user._id) !== -1) {
        return true;
    }

    // user is accepter of request
    if (item instanceof Request && item._accepter && item._accepter.equals(user._id)) {
        return true;
    }

    return false;
}
