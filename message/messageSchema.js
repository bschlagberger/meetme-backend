/**
 * Database schema for a message.
 */

var mongoose = require('mongoose');

var Message = new mongoose.Schema({
    _sender: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    _item: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Item',
        required: true
    },
    timestamp: {
        type: Date,
        required: true
    },
    content: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Message', Message);
