/**
 * Routes definitions for Offers.
 */

module.exports = function (passport) {
    var offerController = require('./offerController');
    var router = require('express').Router();
    var unless = require('express-unless');

    var mw = passport.authenticate('jwt', {session: false});
    mw.unless = unless;

    //middleware TODO: exclude get for filtered routes
    router.use(mw);

    router.route('')
        .post(offerController.postOffer)
        .get(offerController.getOffers);

    router.route('/_search/:query')
        .get(offerController.searchOffer);
    
    router.route('/my_created')
        .get(offerController.getMyOffers);
    
     router.route('/my_accepted')
         .get(offerController.getMyAcceptedOffers);
    
    router.route('/:offer_id')
        .get(offerController.getOffer)
        .post(offerController.updateOffer)
        .delete(offerController.deleteOffer);
    
    return router;
};
