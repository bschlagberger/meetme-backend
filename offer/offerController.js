/**
 * Controller handling CRUD operations for offers.
 */

var Offer = require('./offerSchema');

/**
 * POST /api/offers
 */
exports.postOffer = function(req, res) {
    var newOffer = req.body;
    newOffer._creator = req.user._id;
    newOffer._accepters = [];

    var offer = new Offer(newOffer);

    offer.save(function(err, offer) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        Offer.populate(offer, '_creator', function(err) {
            if (err) {
                res.status(500).send(err);
                return;
            }

            res.status(201).json(offer);
        })
    });
};

/**
 * GET /api/offers
 */
exports.getOffers = function(req, res) {
    Offer
        .find('this._accepters.length +5 < maxNumberOfParticipants')
        .populate('_creator')
        .exec(function(err, offers) {
            if (err) {
                res.status(500).send(err);
                return;
            }

            res.json(offers);
    });
};

/**
 * GET /api/offers/mine
 */
exports.getMyOffers = function (req, res) {
  Offer
      .where('_creator').equals(req.user._id)
      .populate('_accepters')
      .exec(function (err, offers) {
          if(err){
              res.status(500).send(err);
              return;
          }
          res.json(offers);
      });
};

exports.getMyAcceptedOffers = function (req, res) {
    Offer
        .find({_accepters: req.user._id})
        .populate('_creator')
        .exec(function (err, offers) {
            if(err){
                res.status(500).send(err);
                return;
            }
            res.json(offers);
        });
};

// /**
//  * GET /api/offer/_search/:query
//  */
exports.searchOffer = function (req, res) {
    Offer
        .where('title')
        .equals(req.params.query)
        .exec(function (err, offer) {
            if (err) {
                res.status(500).send(err);
                return;
            }

            res.json(offer);
        })
};


/**
 * GET /api/offers/:offer_id
 */
exports.getOffer = function(req, res) {
    Offer
        .findById(req.params.offer_id)
        .populate('_creator')
        // .populate({
        //     path: '_accepters',
        //     populate: {path: '_accepters'}
        // })
        .exec(function(err, offer) {
            if (err) {
                res.status(500).send(err);
                return;
            }

            res.json(offer);
    });
};

/**
 * POST /api/offers/:offer_id
 */
exports.updateOffer = function (req, res) {
    Offer
        .findById(req.params.offer_id)
        .exec(function (err, offer) {
            if (err) {
                res.status(500).send(err);
                return;
            }

            Object.keys(req.body).forEach(function (key) {
                // if currentUser is the creator, he can edit the offer
                if (offer._creator && req.user.equals(offer._creator)) {
                    var allowedFields = [
                        'title',
                        'type',
                        'description',
                        'date',
                        'maxNumberOfParticipants',
                        'price'
                    ];

                    if (allowedFields.indexOf(key) != -1) {
                        offer[key] = req.body[key];
                    }
                }
                // if currentUser is not the creator and there are still less accepter than the max, he can accept the offer
                else if (offer.maxNumberOfParticipants > offer._accepters.length) {
                    var allowedFields = [
                        '_accepters'
                    ];

                    if (allowedFields.indexOf(key) != -1) {
                        offer[key] = req.body[key];
                    }
                }
            });

            offer.save(function (err, offer) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.json(offer);
                }
            });
        });
};

/**
 * DELETE /api/offers/:offer_id
 */
exports.deleteOffer = function(req, res) {
    Offer.findById(req.params.offer_id, function(err, offer) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        if (!offer) {
            res.status(404).send('Could not find any matching offer');
            return;
        }

        //authorize
        if (offer._creator && req.user.equals(offer._creator)) {
            offer.remove();
            res.sendStatus(200);
        } else {
            res.sendStatus(401);
        }
    });
};
