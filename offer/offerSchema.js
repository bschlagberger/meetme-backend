/**
 * Database schema for a offers.
 */

var mongoose = require('mongoose');
var Item = require('../item/itemSchema');

var Offer = new mongoose.Schema({
    type: {
        type: String,
        enum: [
            'Trips',
            'Education',
            'Organization',
            'General'
        ]
    },
    _accepters: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    maxNumberOfParticipants: {
        type: Number,
        required: true
    }
});

module.exports = Item.discriminator('Offer', Offer);
