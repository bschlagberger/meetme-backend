/**
 * Routes definitions for Request.
 */

module.exports = function (passport) {
    var requestController = require('./requestController');
    var router = require('express').Router();
    var unless = require('express-unless');

    var middleware = passport.authenticate('jwt', {session: false});
    middleware.unless = unless;
    // router.use(middleware.unless({method: ['GET', 'OPTIONS']}));
    router.use(middleware);

    router.route('')
        .get(requestController.getRequests)
        .post(requestController.postRequest);


    router.route('/my_created')
        .get(requestController.getMyRequests);

    router.route('/my_accepted')
        .get(requestController.getMyAcceptedRequests);
    
    router.route('/_searchTitle/:query')
        .get(requestController.searchRequestTitle);

    router.route('/_searchDescription/:query')
        .get(requestController.searchRequestDescription);

    router.route('/:request_id')
        .get(requestController.getRequest)
        .post(requestController.updateRequest)
        .delete(requestController.deleteRequest);

    return router;
};
