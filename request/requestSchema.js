/**
 * Database schema for a request.
 */

var mongoose = require('mongoose');
var Item = require('../item/itemSchema');

var Request = new mongoose.Schema({
    type: {
        type: String,
        enum: [
            'Pickup Service',
            'Education',
            'Organization',
            'General'
        ]
    },
    _accepter: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = Item.discriminator('Request', Request);
