/**
 * Controller handling CRUD operations for requests.
 */

var Request = require('./requestSchema');

/**
 * POST /api/requests
 */
exports.postRequest = function (req, res) {
    var newRequest = req.body;
    newRequest._creator = req.user._id;

    var request = new Request(newRequest);

    request.save(function (err, request) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        Request.populate(request, '_creator', function (err) {
            if (err) {
                res.status(500).send(err);
                return;
            }

            res.status(201).json(request);
        })
    });
};

/**
 * GET /api/requests/mine
 */
exports.getMyRequests = function (req, res) {
    Request
        .where('_creator').equals(req.user._id)
        .populate('_accepter')
        .exec(function (err, requests) {
            if (err) {
                res.status(500).send(err);
                return;
            }
            res.json(requests);
        });
};
exports.getMyAcceptedRequests = function (req, res) {
    Request
        .where('_accepter').equals(req.user._id)
        .populate('_creator')
        .exec(function (err, requests) {
            if (err) {
                res.status(500).send(err);
                return;
            }
            res.json(requests);
        });
};

/**
 * GET /api/requests
 */
exports.getRequests = function (req, res) {
    Request
        .where('_accepter').equals(null)
        .populate('_creator')
        .exec(function (err, requests) {
            if (err) {
                res.status(500).send(err);
                return;
            }

            res.json(requests);
        });
};

// /**
//  * GET /api/requests/_searchTitle/:query
//  */
exports.searchRequestTitle = function (req, res) {
    var regexp = new RegExp(req.params.query);
    Request
        .find(
            {'title': { $in: [regexp]}},
            function (err,requests) {
                if (err) {
                    res.status(500).send(err);
                    return;
                }

                res.json(requests);
            }
        )
};


// /**
//  * GET /api/requests/_searchDescription/:query
//  */
exports.searchRequestDescription = function (req, res) {
    var regexp = new RegExp(req.params.query);
    Request
        .find(
            {'description': { $in: [regexp]}},
            function (err,requests) {
                if (err) {
                    res.status(500).send(err);
                    return;
                }

                res.json(requests);
            }
        )
};


/**
 * GET /api/requests/:request_id
 */
exports.getRequest = function (req, res) {
    Request
        .findById(req.params.request_id)
        .populate('_creator')
        .populate('_accepter')
        .exec(function (err, request) {
            if (err) {
                res.status(500).send(err);
                return;
            }

            res.json(request);
        });
};

/**
 * POST /api/requests/:request_id
 */
exports.updateRequest = function (req, res) {
    Request
        .findById(req.params.request_id)
        .exec(function (err, request) {
            if (err) {
                res.status(500).send(err);
                return;
            }
            var allowedFields = [
                'title',
                'type',
                'description',
                'date',
                'price'
            ];

            if (request._creator && req.user.equals(request._creator)) {
                // if currentUser is the creator, he can edit the request

                Object.keys(req.body).forEach(function (key) {
                    if (allowedFields.indexOf(key) != -1) {
                        request[key] = req.body[key];
                    }
                });

            } else if (!request._accepter || request._accepter.equals(req.user._id)) {
                // if currentUser is not the creator and the accepter is still empty, or the user is the accepter he
                // can modify the accepter of the request
                if (req.body._accepter) {
                    request._accepter = req.body._accepter;
                } else {
                    delete request._accepter;
                    Request.update({_id: req.body._id}, {$unset: {_accepter: ""}}, function (err) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.json(request);
                        }
                    });
                    return;
                }
            }

            request.save(function (err, request) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.json(request);
                }
            });
        });
};

/**
 * DELETE /api/requests/:request_id
 */
exports.deleteRequest = function (req, res) {
    Request.findById(req.params.request_id, function (err, request) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        if (!request) {
            res.status(404).send('Could not find any matching request');
            return;
        }

        //authorize
        if (request._creator && req.user.equals(request._creator)) {
            request.remove();
            res.sendStatus(200);
        } else {
            res.sendStatus(401);
        }
    });
};
