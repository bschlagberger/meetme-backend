/**
 * Database schema for a classifiedAd
 */
var util = require('util');
var mongoose = require('mongoose');

function AbstractItemSchema() {
    mongoose.Schema.apply(this, arguments);
    this.add({
        title: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        date: {
            type: Date,
            required: false
        },
        _creator: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        price: {
            type: Number,
            required: false
        }
        // TODO: time
        // TODO: price
    });
};

util.inherits(AbstractItemSchema, mongoose.Schema);

var ItemSchema = new AbstractItemSchema();
module.exports = mongoose.model('Item', ItemSchema);
