var Config = require('./config/config.js');

/**
 * DB connect
 */
var mongoose = require('mongoose');
mongoose.connect([Config.db.host, '/', Config.db.name].join(''), {
    //eventually it's a good idea to make this secure
    user: Config.db.user,
    pass: Config.db.pass
});

/**
 * create application
 */
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');

var app = express();

/**
 * app setup
 */
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


// Passport
var passport = require('passport');
var jwtConfig = require('./passport/jwtConfig');

app.use(passport.initialize());
jwtConfig(passport);


/**
 * Routing
 */
var authRoutes = require('./user/authRoutes');
var userRoutes = require('./user/userRoutes');
var requestRoutes = require('./request/requestRoutes');
var offerRoutes = require('./offer/offerRoutes');
var messageRoutes = require('./message/messageRoutes');

app.use('/', authRoutes(passport));
app.use('/api/user', userRoutes(passport));
app.use('/api/requests', requestRoutes(passport));
app.use('/api/offers', offerRoutes(passport));
app.use('/api/messages', messageRoutes(passport));

// For debugging purposes.
app.get('/', function (req, res) {
    res.send('It works!');
});

module.exports = app;
