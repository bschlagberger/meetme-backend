/**
 * Route definitions for the user controller.
 */

module.exports = function (passport) {
    var userController = require('./userController');
    var router = require('express').Router();

   router.use(passport.authenticate('jwt', {session: false}));

    router.route('')
        .get(userController.getMe);

    router.route('/:user_id')
        .post(userController.updateUser)
        .get(userController.getUser);

    return router;
};
