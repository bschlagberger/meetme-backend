/**
 * Route definitions for the authentication controller.
 */

module.exports = function (passport) {
    var authController = require('./authController');
    var router = require('express').Router();

    router.post('/login', authController.login);
    router.post('/signup', authController.signup);
    router.post('/unregister', passport.authenticate('jwt', {session: false}), authController.unregister);
    router.post('/password', passport.authenticate('jwt', {session: false}), authController.updatePassword);

    return router;
};
