var Config = require('../config/config.js');
var User = require('./userSchema');
var jwt = require('jwt-simple');

/**
 * POST /login
 *
 * Generates the token for the given email/password combination.
 */
exports.login = function (req, res) {
    if (!req.body.email) {
        res.status(400).send('email required');
        return;
    }
    if (!req.body.password) {
        res.status(400).send('password required');
        return;
    }

    User.findOne({email: req.body.email})
        .select('password firstname lastname email')
        .exec(function (err, user) {
            if (err) {
                res.status(500).send(err);
                return
            }

            if (!user) {
                res.status(401).send('Invalid Credentials');
                return;
            }

            user.comparePassword(req.body.password, function (err, isMatch) {
                if (!isMatch || err) {
                    res.status(401).send('Invalid Credentials');
                } else {
                    res.status(200).json({token: createToken(user)});
                }
            });
        });

};

/**
 * POST /signup
 *
 * Creates a new user
 */
exports.signup = function (req, res) {
    var user = new User(req.body);

    user.save(function (err) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.status(201).json({token: createToken(user)});
    });
};

/**
 * POST /unregister
 *
 * Deletes the logged in user
 */
exports.unregister = function (req, res) {
    req.user.remove().then(function () {
        res.sendStatus(200);
    }, function (err) {
        res.status(500).send(err);
    });
};

/**
 * PUT /password
 *
 * Updates the password of the logged in user.
 */
exports.updatePassword = function (req, res) {
    User.findById(req.user._id)
        .select('password')
        .exec(function (err, user) {
            user.password = req.body.password;
            user.save(function(err, user) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.json(user);
                }
            });
        });
};

function createToken(user) {
    return jwt.encode(user._id, Config.auth.jwtSecret);
}
