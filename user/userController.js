var User = require('./userSchema');

/**
 * GET /api/user
 *
 * Returns the logged in user
 */
exports.getMe = function (req, res) {
    User.findById(req.user._id)
        .exec(function (err, user) {
            if (err) {
                res.status(500).send(err);
                return;
            }

            res.json(user);
        });
};

/**
 * GET /api/user/:user_id
 *
 * Returns the user defined by the given id.
 */
exports.getUser = function (req, res) {
    User.findById(req.params.user_id)
        .select('firstname lastname country picture')
        .exec(function (err, user) {
            if (err) {
                res.status(500).send(err);
                return;
            }

            res.json(user);
        });
};

/**
 * PUT /api/user/:user_id
 *
 * Updates the logged in user.
 */
exports.updateUser = function (req, res) {
    if(!req.user._id.equals(req.params.user_id)){
        res.status(403).send('unauthorised');
        return;
    }
    User.findById(req.params.user_id)
        .exec(function (err, user) {
            if(err) {
                res.status(500).send(err);
                return;
            }

            Object.keys(req.body).forEach(function(key) {
                var allowedFields = [
                    'email',
                    'firstname',
                    'lastname',
                    'country',
                    'picture'
                ];
                
                if (allowedFields.indexOf(key) != -1) {
                    user[key] = req.body[key];
                }
            });

            user.save(function (err, user) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.json(user);
                }
            });
        });
};
